echo "*****************************************************************"
echo "CREATE VIM REPOSITORIES"
echo "*****************************************************************"

mkdir -p ~/.vim/autoload
mkdir -p ~/.vim/colors
mkdir -p ~/.vim/syntax
mkdir -p ~/.vim/plugin
mkdir -p ~/.vim/plugged
mkdir -p ~/.vim/spell
mkdir -p ~/.vim/config
mkdir -p ~/.vim/vimrc

echo "*****************************************************************"
echo "COPY VIM FILES"
echo "*****************************************************************"
cp .vimrc ~/
cp *_settings.vim ~/.vim/vimrc
cp color/gruvbox.vim ~/.vim/colors


echo "*****************************************************************"
echo "INSTALL VIM PLUG"
echo "*****************************************************************"
# install vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "*****************************************************************"
echo "UPDATE DEBIAN PACKAGE"
echo "*****************************************************************"
sudo apt update

echo "*****************************************************************"
echo "INSTALL DOCKER"
echo "*****************************************************************"
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt-cache policy docker-ce
sudo apt install docker-ce
sudo usermod -aG docker $(whoami)

echo "*****************************************************************"
echo "INSTALL DOCKER-COMPOSE"
echo "*****************************************************************"
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "*****************************************************************"
echo "INSTALL NODEJS"
echo "*****************************************************************"
#install nodejs
sudo apt install nodejs
sudo apt install npm

echo "*****************************************************************"
echo "INSTALL INTELEPHENSE"
echo "*****************************************************************"
#install intelephense LSP PHP
npm -g install intelephense

echo "*****************************************************************"
echo "INSTALL PHP"
echo "*****************************************************************"
#install php
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt install php7.3 php7.3-common php7.3-opcache php7.3-cli php7.3-gd php7.3-curl php7.3-mysql php7.3-xml php7.3-mbstring

echo "*****************************************************************"
echo "INSTALL COMPOSER"
echo "*****************************************************************"
# install composer
cd /usr/local/bin
sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php -r "if (hash_file('sha384', 'composer-setup.php') === 'baf1608c33254d00611ac1705c1d9958c817a1a33bce370c0595974b342601bd80b92a3f46067da89e3b06bff421f182') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php
sudo php -r "unlink('composer-setup.php');"
sudo mv composer.phar composer


echo "*****************************************************************"
echo "INSTALL RIPGREP"
echo "*****************************************************************"
#install ripgrep
sudo apt-get install ripgrep

echo "*****************************************************************"
echo "INSTALL FZF"
echo "*****************************************************************"
#install fzf
sudo apt-get install fzf

echo "*****************************************************************"
echo "INSTALL CTAGS"
echo "*****************************************************************"
#install ctags
git clone https://github.com/universal-ctags/ctags.git
cd ctags
./autogen.sh 
./configure
make
sudo make install

echo "*****************************************************************"
echo "INSTALL GIT"
echo "*****************************************************************"
sudo apt-get install git


echo "*****************************************************************"
echo "CHANGE OWNER REPOSITORY .vim"
echo "*****************************************************************"
cd ~
sudo chown -R $(whoami):$(whoami) .vim
