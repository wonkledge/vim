call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-abolish'
Plug 'moll/vim-bbye'
Plug 'tpope/vim-commentary' 
Plug 'scrooloose/nerdtree' 
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete-file.vim'
Plug 'yami-beta/asyncomplete-omni.vim'
Plug 'prabirshrestha/asyncomplete-buffer.vim'
call plug#end()

source ~/.vim/vimrc/plugin_settings.vim
source ~/.vim/vimrc/basic_settings.vim
source ~/.vim/vimrc/mapping_settings.vim
source ~/.vim/vimrc/function_settings.vim



