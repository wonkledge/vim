let mapleader = ","
nnoremap <leader>w :w!<CR>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <leader>t :Tags<CR>
nnoremap <leader>f :FZF<CR>
nnoremap <leader>m :Marks<CR>
nnoremap <leader>c :Commits<CR>
nnoremap <leader>, :Bdelete!<CR>
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader>. :q!<CR>
nnoremap <leader><space> :nohlsearch<CR>
" Comment
nnoremap <C-c> gcc

" surround with "
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lel

" FUGITIVE MAPPING
nnoremap <leader>b :Gblame<CR>
nnoremap <leader>d :Gdiffsplit<CR>
nnoremap <leader>g :Gstatus<CR>

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"
inoremap <c-space> <Plug>(asyncomplete_force_refresh)
